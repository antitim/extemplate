# -*- coding: utf-8 -*-
import os
import shutil
import json
from jinja2 import Template


def email(folder):
    print('Папка: ' + folder)
    shutil.copytree('./template/', './result/' + folder)

    for item in os.listdir('./content/' + folder):
        shutil.copyfileobj(
            open('./content/' + folder + '/' + item, 'rb'),
            open('./result/' + folder + '/' + item, 'wb')
        )

    if 'content.json' in os.listdir('./content/' + folder):
        template_file = open('./result/' + folder + '/index.html', 'r', encoding='utf-8')
        content = open('./content/' + folder + '/content.json', 'r', encoding='utf-8')
        content = json.load(content)

        template = Template(template_file.read())
        result = template.render(content)
        template_file.close()

        index_file = open('./result/' + folder + '/index.html', 'w', encoding='utf-8')
        index_file.write(result)
        index_file.close()


def generate():
    try:
        shutil.rmtree('./result/')
    except:
        print('Не возможно удалить папку с результатами')

    os.mkdir('./result/')

    os.listdir('./content')

    for i in os.listdir('./content'):
        email(i)

    print('Выполнено')
