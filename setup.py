from setuptools import setup

setup(
    name='exTemplate',
    version='',
    packages=['extemplate'],
    url='',
    license='',
    author='antitim',
    author_email='antitim@yandex.ru',
    description='',
    requires=['jinja2'],
    entry_points={
        'console_scripts': [
            'xt_generate = extemplate.main:generate',
        ]
    }
)
